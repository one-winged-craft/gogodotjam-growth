# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name AbsorberBullet
extends BaseBullet


export (float) var range_shot: = PI / 4
export (int) var radius: = 25


var _direction: = 0.0


func _ready():
	_direction = range_shot * _id - range_shot / 2 


func _move(delta: float) -> void:
	position += transform.rotated(_direction).x * speed * delta


func _on_AbsorberBullet_area_entered(area: BaseBullet) -> void:
	if is_instance_valid(area) and area.is_in_group("mob bullet"):
		area.queue_free()

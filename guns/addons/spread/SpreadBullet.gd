# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name SpreadBullet, "res://guns/addons/spread/SpreadBullet.png"
extends BaseBullet


export (float) var random_burst_range: = 0.5 #PI / 8
export (int) var min_speed: = 200
export (int) var decceleration: = 5


var _direction: = 0.0


func _ready():
	_direction = randf() * random_burst_range - random_burst_range / 2


func _move(delta):
	speed = int(max(min_speed, speed - decceleration))
	position += transform.rotated(_direction).x * speed * delta

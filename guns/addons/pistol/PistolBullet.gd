# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name PistolBullet
extends BaseBullet


func _ready():
	position.y += randi() % 5 - 2

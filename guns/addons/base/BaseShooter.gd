# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name BaseShooter
extends Position2D


signal shot()
signal left_bursts(burst_left)
signal loading(progress)


export (PackedScene) var Bullet
export (float) var rate: = 0.6 # 0 to tap to shoot, -1 to shoot on release
export (float) var load_time: = 0.0
export (int) var ammo: = 1
export (int) var ammo_per_burst: = 0
export (float) var interval: = 0.0


var is_shooting: = false
var _loading: = false
var _load_time_acc: = 0.0


onready var _rate_timer: = get_tree().create_timer(0.0)


func _ready() -> void:
	_emit_left_bursts()

func _process(delta):
	if load_time > 0.0 and _loading:
		emit_signal("loading", 1 - _load_time_acc / load_time)
		_load_time_acc += delta


func end_shooting() -> void:
	if rate < 0.0 \
			and (load_time == 0.0 or _load_time_acc <= load_time):
		_shoot()

	_loading = false
	_load_time_acc = 0.0


func shooting() -> void:
	if rate > 0.0 \
			and _rate_timer.time_left <= 0.0 and _load_time_acc >= load_time:
		_rate_timer = get_tree().create_timer(rate)
		_shoot()

	elif rate == 0.0 \
			and _load_time_acc == 0.0:
		_load_time_acc = 1.0
		_shoot()

	_loading = true


func _shoot() -> void:
	if ammo <= 0:
		return

	ammo -= ammo_per_burst
	_create_bullets()
	_emit_left_bursts()
	emit_signal("shot")


func _emit_left_bursts() -> void:
	# warning-ignore:integer_division
	emit_signal("left_bursts", ammo / ammo_per_burst) # division by 0 error will catch you!


func _create_bullets() -> void:
	if is_shooting:
		return

	if ammo_per_burst <= 1:
		get_node("/root/Root/Bullets").add_child(_bullet_instance())
		return

	is_shooting = true
	for i in ammo_per_burst:
		if interval > 0.0:
			yield(get_tree().create_timer(interval), "timeout")
		get_node("/root/Root/Bullets").add_child(
				_bullet_instance().id(i / float(ammo_per_burst - 1)))
	is_shooting = false


func _bullet_instance() -> BaseBullet:
	return Bullet.instance().to(global_position).towards(transform.x)

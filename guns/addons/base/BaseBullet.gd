# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name BaseBullet, "res://guns/addons/base/BaseBullet.png"
extends Area2D


export (float) var speed: = 400
export (int) var damage: = 1


var _id: float


func _ready() -> void:
	# warning-ignore:return_value_discarded
	self.connect("body_entered", self, "_on_Bullet_body_entered")
	self.add_to_group("player bullet")
	position.y += randi() % 2


func _physics_process(delta: float) -> void:
	_move(delta)


func to(position: Vector2) -> BaseBullet:
	self.position = position
	return self


func towards(direction: Vector2) -> BaseBullet:
	self.transform.x = direction
	return self


# warning-ignore:shadowed_variable
func with_damage(damage: int) -> BaseBullet:
	self.damage = damage
	return self


func id(id: float) -> BaseBullet:
	self._id = id
	return self


func _move(delta) -> void:
	position += transform.x * speed * delta


func _on_Bullet_body_entered(body: Node):
	if body.is_in_group("mobs"):
		body.take_damage(damage)
		self.queue_free()
		# warning-ignore:return_value_discarded
		var find_canonical_name: = RegEx.new() # In regex we trust. Or do we?
		find_canonical_name.compile("@(\\w+)@\\d+")
		var canonical_name: = find_canonical_name.search(name).get_string(1) if find_canonical_name.search(name) else name
		Effect.one_shot_anim(
				get_tree().root,
				canonical_name + "_impact",
				global_position)

	if body.is_in_group("walls"):
		self.queue_free()

# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name MissileBullet, "res://guns/addons/missile/MissileBullet.png"
extends BaseBullet


export (int) var max_speed: = 400
export (int) var acceleration: = 4
export (float) var rotation_speed: = PI / 30


var target: Node = null


var _direction: = - Vector2.ONE if randi() % 2 else Vector2(-1, 1)


func _move(delta: float) -> void:
	if target == null:
		target = _get_closest_enemy()

	speed = int(min(max_speed, speed + acceleration))
	rotation_speed += PI / 200 * rotation_speed
	if is_instance_valid(target):
		_target(target.global_position)
	else:
		_target(
			get_viewport().get_visible_rect().size * Vector2(1.5, 0.5))
	position += _direction * speed * delta


func _target(point: Vector2) -> void:
	_direction = _direction.rotated(
			_direction.angle_to(
			position.direction_to(point)) * rotation_speed)


func _get_closest_enemy() -> Node:
	var closest: = { "distance": INF, "enemy": null }

	for enemy in get_tree().get_nodes_in_group("mobs"):
		var distance: = self.position.distance_squared_to(enemy.global_position)
		if closest["distance"] > distance:
			closest["distance"] = distance
			closest["enemy"] = enemy

	return closest["enemy"]

# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

extends Line2D


export(int) var trail_length


func _process(_delta):
	add_point(owner.global_position, 0)
	if get_point_count() > trail_length :
		remove_point(get_point_count()-1)

# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name LaserShooter
extends BaseShooter


func _create_bullets() -> void:
	add_child(Bullet.instance())
	_emit_left_bursts()

# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name Laser
extends BaseBullet


export (float) var radius: = 16
export (float) var lifetime: = 1
export (float) var damage_interval: = 0.1


func _ready():
	get_parent().is_shooting = true
	$Tween.stop_all()
	$Tween.interpolate_property($Beam/Tail, "width", 0, radius, 0.2)
	$Tween.interpolate_property($Beam/Head, "width", 0, radius, 0.2)
	$Tween.start()
	yield($Tween, "tween_completed")

	$LaserSpeedTimer.set_wait_time(damage_interval)
	$LaserSpeedTimer.start()

	$Hitbox.polygon[0] = Vector2(_get_laser_length(), - radius / 2.0)
	$Hitbox.polygon[1] = Vector2(_get_laser_length(), radius / 2.0)
	$Hitbox.polygon[2] = Vector2(0, radius / 2.0)
	$Hitbox.polygon[3] = Vector2(0, - radius / 2.0)

	yield(get_tree().create_timer(lifetime), "timeout")

	for count in 4:
		$Hitbox.polygon[count] = Vector2.ZERO

	$Tween.stop_all()
	$Tween.interpolate_property($Beam/Tail, "width", radius, 0, 0.5)
	$Tween.interpolate_property($Beam/Head, "width", radius, 0, 0.5)
	$Tween.start()
	yield($Tween, "tween_completed")

	get_parent().is_shooting = false
	self.queue_free()


func _move(_delta):
	$Beam.cast_to = Vector2.RIGHT * _get_laser_length()
	$Beam/Tail.points[0] = $Beam/Head.points[1]
	$Beam/Tail.points[1] = $Beam.cast_to


func _on_Bullet_body_entered(_body: Node):
	pass


func _get_laser_length() -> float:
	return get_viewport().get_visible_rect().size.x - self.global_position.x


func _on_LaserSpeedTimer_timeout():
	for catched in get_overlapping_bodies():
		if catched.is_in_group("mobs"):
			catched.take_damage(damage)

# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name DragonBullet
extends MobBaseBullet


export (float) var range_shot: = PI / 4


var _direction: = 0.0


func _ready():
	_direction = range_shot * _id - range_shot / 2 


func _move(delta):
	position += transform.rotated(_direction).x * speed * delta


func _on_Bullet_body_entered(body: Node) -> void:
	if body.is_in_group("walls"):
		self.queue_free()

	if body.is_in_group("weakpoint"):
		body.get_parent().die()
		self.queue_free()

	if body.is_in_group("addons"):
		body.get_parent().take_damage(damage)
		self.queue_free()

	if body.is_in_group("addons"):
		Effect.one_shot_anim(get_tree().root, "Dragon_impact", global_position)

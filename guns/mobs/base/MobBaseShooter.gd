# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name MobBaseShooter
extends Position2D


signal shot


export (PackedScene) var Bullet
export (bool) var auto_shoot: = true
export (float) var rate: = 0.6
export (int) var ammo_per_burst: = 0
export (float) var interval: = 0.0
export (Vector2) var direction: = Vector2.LEFT


var is_shooting: = false
var _load_time_acc: = 0.0


onready var _rate_timer: = get_tree().create_timer(0.0)


func _process(_delta: float) -> void:
	if _rate_timer.time_left <= 0.0 and auto_shoot:
		_create_bullets()
		_rate_timer = get_tree().create_timer(rate)
	elif not auto_shoot:
		_rate_timer.time_left = 0


func _create_bullets() -> void:
	if ammo_per_burst <= 1:
		get_node("/root/Root/Bullets").add_child(_bullet_instance())
		return

	is_shooting = true
	for bullet in ammo_per_burst:
		if interval > 0.0:
			yield(get_tree().create_timer(interval), "timeout")
		get_node("/root/Root/Bullets").add_child(
				_bullet_instance().id(bullet / float(ammo_per_burst - 1)))
	is_shooting = false


func _bullet_instance() -> MobBaseBullet:
	emit_signal("shot")
	return Bullet.instance().to(global_position).towards(direction)

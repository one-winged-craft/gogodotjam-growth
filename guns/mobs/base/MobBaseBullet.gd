# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name MobBaseBullet, "res://guns/mobs/base/MobBaseBullet.png"
extends BaseBullet


func _ready() -> void:
	self.remove_from_group("player bullet")
	self.add_to_group("mob bullet")


func _on_Bullet_body_entered(body: Node) -> void:
	if body.is_in_group("walls"):
		self.queue_free()

	if body.is_in_group("weakpoint"):
		body.get_parent().die()
		self.queue_free()

	if body.is_in_group("addons"):
		body.get_parent().take_damage(damage)
		self.queue_free()

	if body.is_in_group("addons"):
		Effect.one_shot_anim(get_tree().root, "Base_impact", global_position)

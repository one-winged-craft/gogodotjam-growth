# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

extends MobBaseShooter


func _bullet_instance() -> MobBaseBullet:
	emit_signal("shot")
	return Bullet.instance().to(global_position).towards(
			global_position.direction_to(
			get_tree().get_nodes_in_group("weakpoint")[0].global_position))

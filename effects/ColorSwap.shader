// SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
// SPDX-FileCopyrightText: 2021 Luc Deligne
//
// SPDX-License-Identifier: MIT

shader_type canvas_item;

uniform vec4 new_color : hint_color = vec4(0, 1, 0, 1);
uniform vec4 old_color : hint_color = vec4(1, 1, 1, 1);

void fragment() {
	vec4 curr_color = texture(TEXTURE,UV);

	if (curr_color == old_color) {
        COLOR = new_color;
	} else {
        COLOR = curr_color;
    }
}
# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name OrbHandler
extends Node2D


var number_of_orbs: = 8
var speed: = 200
var slowing_speed: = 12
var directions: = []



func _ready() -> void:
	for count in number_of_orbs:
		Effect.one_shot_anim(self, "death_orb")
		directions.append(Vector2.RIGHT.rotated(TAU / number_of_orbs * count).normalized())


func _process(delta: float) -> void:
	if get_child_count() == 0:
		self.queue_free()

	for count in get_child_count():
		get_child(count).position += directions[count] * speed * delta
	# warning-ignore:narrowing_conversion
	speed = max(0, speed - slowing_speed)


func to(position: Vector2) -> OrbHandler:
	self.position = position
	return self 

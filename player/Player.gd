# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name Player
extends KinematicBody2D


signal game_over


const ADDON_MAX = 15
const OrbHandler = preload("res://effects/OrbHandler.tscn")
const split: = 5
const priority: = {
	"idle": 0, "shoot_self": 0,
	"down": 10, "up": 10, "back": 20, "forward": 20,
	"throw_addon": 60, "get_addon": 60, "shoot_addon": 70
}


export (NodePath) var tail
export (int) var maximum_speed: = 160
export (int) var acceleration: = 30
export (int) var decceleration: = 15


var speed: = 0.0
var input: = Vector2.ZERO setget set_input


var _direction: = Vector2.RIGHT
var _is_playing: = false


onready var size: float = $Hitbox.shape.radius * 2 + 4
onready var _timer: = get_tree().create_timer(0.0)


func _ready() -> void:
	get_node(tail).curve.set_bake_interval(size / split)
	get_node(tail).curve.add_point(position)


func _physics_process(_delta: float) -> void:
	if input != Vector2.ZERO:
		_direction = input
		speed = _approach(speed, maximum_speed, acceleration)
	else:
		speed = _approach(speed, 0.0, decceleration)

	# warning-ignore:return_value_discarded
	move_and_slide(speed * _direction)
	_set_move_animation(input)

	if get_node(tail).curve.get_point_count() > 0 and \
			position.round() != get_node(tail).curve.get_point_position(0).round():
		_move_tail()


func set_input(i: Vector2) -> void:
	input = _not_towards_tail(i.normalized(), PI / 4)


func die() -> void:
	get_tree().root.add_child(OrbHandler.instance().to(global_position))
	Effect.one_shot_anim(get_tree().root, "death_flash", global_position)
	position = Vector2(-100, -100)
	emit_signal("game_over")


func push_tail(addon: Node) -> void:
	if get_node(tail).get_child_count() >= ADDON_MAX:
		addon.queue_free()
		return

	request_anim("get_addon")
	for s in split:
		get_node(tail).curve.add_point(_get_tail_direction())
	_update_addon_position(addon, get_node(tail).get_child_count())
	get_node(tail).call_deferred("add_child", addon)


func pop_tail() -> Node:
	if get_node(tail).get_child_count() == 0:
		return null

	request_anim("throw_addon")
	var addon: = get_node(tail).get_child(get_node(tail).get_child_count() - 1)
	for s in split:
		get_node(tail).curve.remove_point(get_node(tail).curve.get_point_count() - 1)
	addon.position = addon.global_position
	get_node(tail).remove_child(addon)
	return addon


func clear_tail() -> void:
	get_node(tail).curve.clear_points()
	for addon in get_node(tail).get_children():
		addon.explode()
		yield(get_tree().create_timer(0.2), "timeout")
	for addon in get_node(tail).get_children():
		addon.queue_free()
	yield(get_tree().create_timer(0.5), "timeout")


func can_shoot() -> bool:
	for addon in get_node(tail).get_children():
		if is_instance_valid(addon.shooter) and addon.shooter.ammo > 0:
			return true
	return false


func request_anim(animation: String) -> void:
	if animation == $Sprite.animation:
		if $Sprite.frames.get_frame_count($Sprite.animation) - 1 == $Sprite.frame:
			$Sprite.frame = 0
		return

	if priority[animation] >= priority[$Sprite.animation] or \
			$Sprite.frame == $Sprite.frames.get_frame_count($Sprite.animation) - 1:
		_is_playing = true
		$Sprite.play(animation)


func _set_move_animation(direction: Vector2) -> void:
	if direction.x > 0.0:
		request_anim("forward")
	elif direction.x < 0.0:
		request_anim("back")
	elif direction.y > 0.0:
		request_anim("down")
	elif direction.y < 0.0:
		request_anim("up")
	else:
		request_anim("idle")


func _approach(start: float, end: float, step: float) -> float:
	if start < end: 
		return min(start + step, end)
	else:
		return max(start - step, end)


func _move_tail() -> void:
	get_node(tail).curve.set_point_position(0, position)

	for addon in get_node(tail).get_children():
		_update_addon_position(addon, addon.get_index())


func _update_addon_position(addon: Node, idx: int) -> void:
	var offset: = idx * split + 1
	for point in range(offset, offset + split):
		addon.offset = size / split * point
		if addon.get_parent() == get_node(tail) and \
				point > 0 and point < get_node(tail).curve.get_point_count():
			get_node(tail).curve.set_point_position(point, addon.global_position)


func _get_tail_direction() -> Vector2:
	var point_count = get_node(tail).curve.get_point_count()

	if point_count < 2:
		return position + Vector2.UP * size / split
	else:
		return get_node(tail).curve.get_point_position(point_count - 1) + \
			get_node(tail).curve.get_point_position(point_count - 2).direction_to(
					get_node(tail).curve.get_point_position(point_count - 1)
			) * size / split


func _not_towards_tail(vector: Vector2, constraint: float) -> Vector2:
	if get_node(tail).get_child_count() == 0 \
			or abs(position.direction_to(
					get_node(tail).curve.get_point_position(1)).angle_to(vector)
			) >= constraint:
		return vector

	return Vector2.ZERO


func _on_Shooter_shot() -> void:
	if input == Vector2.ZERO:
		request_anim("shoot_self")

	Effect.one_shot_anim($Shooter, "BaseShooter_muzzle")

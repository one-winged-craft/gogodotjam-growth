<!--
SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
SPDX-FileCopyrightText: 2021 Luc Deligne

SPDX-License-Identifier: MIT
-->

# Stash'em up

This is our submission for the gogodotjam of 2021. The theme of the jam was
“GROWTH”, and the period was of 10 days.

Please note that every art assets are licensed under the CC-BY license. As such,
the code is licensed under MIT.

You can grab the source code at
https://codeberg.org/one-winged-craft/gogodotjam-growth

Hope you liked it ;-) 

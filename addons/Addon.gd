# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name Addon
extends PathFollow2D


const empty_color: = Color.black
const skins: = {
	"MissileShooter": {
		"color": Color("#00f200"),
		"texture": preload("res://addons/sprites/AddonMissile.png"),
		"hit": preload("res://addons/sprites/AddonHitMissile.png"),
		"shoot": preload("res://addons/sprites/AddonShootMissile.png"),
	},
	"LaserShooter": {
		"color": Color("#00d9ff"), 
		"texture": preload("res://addons/sprites/AddonLaser.png"),
		"hit": preload("res://addons/sprites/AddonHitLaser.png"),
		"shoot": preload("res://addons/sprites/AddonShootLaser.png"),
	},
	"AbsorberShooter": {
		"color": Color("#ff00ff"),
		"texture": preload("res://addons/sprites/AddonAbsorber.png"),
		"hit": preload("res://addons/sprites/AddonHitAbsorber.png"),
		"shoot": preload("res://addons/sprites/AddonShootAbsorber.png"),
	},
	"SpreadShooter": {
		"color": Color("#ffcc00"),
		"texture": preload("res://addons/sprites/AddonSpread.png"),
		"hit": preload("res://addons/sprites/AddonHitSpread.png"),
		"shoot": preload("res://addons/sprites/AddonShootSpread.png"),
	},
	"PistolShooter": {
		"color": Color("#ff6c6c"),
		"texture": preload("res://addons/sprites/AddonPistol.png"),
		"hit": preload("res://addons/sprites/AddonHitPistol.png"),
		"shoot": preload("res://addons/sprites/AddonShootPistol.png"),
	},
	"dead": {
		"color": Color("#3b265b"),
		"texture": preload("res://addons/sprites/AddonDead.png"),
		"damage": preload("res://addons/sprites/AddonDead.png"),
	},
}


export (int) var life: = 3
export (float) var falling_base_speed: = 10.0
export (float) var falling_acceleration: = 1.2


var shooter: BaseShooter
var _direction: = Vector2.UP * 100
var _is_falling: = false


onready var _timer: = get_tree().create_timer(0.0)


func _process(_delta: float) -> void:
	if Input.is_action_pressed("game_shoot") and is_instance_valid(shooter):
		shooter.shooting()
	elif Input.is_action_just_released("game_shoot") and is_instance_valid(shooter):
		 shooter.end_shooting()

	if _timer.time_left <= 0.0 and is_instance_valid(shooter):
		$Body/Sprite.texture = skins[shooter.name]["texture"]


func _physics_process(delta: float) -> void:
	if not _is_falling:
		return

	if not _direction == Vector2.DOWN:
		_direction = _direction.rotated(
					PI / 60.0 * _direction.angle_to(
					Vector2(- 0.00001, 1.0))).normalized() * falling_acceleration

	$Body.position = _direction * falling_base_speed * delta
	falling_base_speed *= falling_acceleration

	if not is_instance_valid(shooter):
		return

	shooter.position = $Body.position
	var outscreen: float \
			= get_viewport().get_visible_rect().size.y + $Body/Hitbox.shape.radius
	if $Body.global_position.y > outscreen and not shooter.is_shooting:
		self.queue_free()


func falling() -> Addon:
	_is_falling = true
	return self


# warning-ignore:shadowed_variable
func set_shooter(shooter: BaseShooter) -> Addon:
	if is_instance_valid(self.shooter):
		shooter.disconnect("left_bursts", self, "_on_Shooter_left_bursts")
		self.shooter.queue_free()

	$Body/Sprite.texture = skins[shooter.name]["texture"]
	$Body/Sprite.material.set_shader_param(
			"new_color", skins[shooter.name]["color"])
	if shooter.name == "MissileShooter":
		$Body/Sprite/Numbers.position += Vector2.LEFT

	self.shooter = shooter
	# warning-ignore:return_value_discarded
	self.shooter.connect("left_bursts", self, "_on_Shooter_left_bursts")
	# warning-ignore:return_value_discarded
	self.shooter.connect("shot", self, "_on_Shooter_shot")

	add_child(shooter)
	return self


func take_damage(damage: int) -> void:
	life -= damage
	$Body/Sprite.texture = skins[shooter.name]["hit"]
	_timer = get_tree().create_timer(0.2)
	if life <= 0:
		explode()


func explode() -> void:
	if not $Weakpoint.is_in_group("addons"):
		yield(get_tree(), "idle_frame")
		return

	shooter.queue_free()
	$Weakpoint.remove_from_group("addons")
	$Body/Sprite.texture = skins["dead"]["texture"]
	$Body/Sprite.material.set_shader_param("new_color", skins["dead"]["color"])
	$Body/Sprite/Numbers.queue_free()

	yield(Effect.one_shot_anim(self, "addon_explosion", Vector2(-8,0)), "completed")


func _on_Shooter_left_bursts(left: int) -> void:
	$Body/Sprite/Numbers.frame = left
	if left == 0:
		$Body/Sprite.material.set_shader_param("new_color", empty_color)


func _on_Shooter_shot() -> void:
	$Body/Sprite.texture = skins[shooter.name]["shoot"]
	_timer = get_tree().create_timer(0.1)
	if shooter.name == "LaserShooter":
		return
	Effect.one_shot_anim(shooter,
			shooter.name + "_muzzle", shooter.transform.x + Vector2(3, 0))

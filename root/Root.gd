# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

extends Node


const RestartMessage: = preload("res://root/RestartMessage.tscn")


export (PackedScene) var debug_shooter


var game_over: = true
var kills: = 0


func _ready() -> void:
	$MobSpawner.stop_spawning()
	add_child(RestartMessage.instance())


func _process(_delta: float) -> void:
	if game_over:
		if is_instance_valid(get_node_or_null("RestartMessage")) and \
				Input.is_action_just_pressed("game_shoot"):
			game_over = false
			kills = 0
			$MobSpawner.start_spawning()
			$RestartMessage.queue_free()
			$Player.position = Vector2(80, 140)
			$Player/Shooter.auto_shoot = true
			$Player.clear_tail()
			for mob in $MobSpawner.get_children():
				mob.queue_free()
			for bullet in $Bullets.get_children():
				bullet.queue_free()
		return

	if Input.is_action_just_pressed("game_shoot") and $Player.can_shoot():
		$Player.request_anim("shoot_addon")

	$Player.input = Vector2(
			int(Input.is_action_pressed("ui_right")) - int(Input.is_action_pressed("ui_left")),
			int(Input.is_action_pressed("ui_down")) - int(Input.is_action_pressed("ui_up")))

	if Input.is_action_just_pressed("game_throw_addon") \
			and $Tail.get_child_count() > 0:
		self.add_child($Player.pop_tail().falling())


func _input(event: InputEvent) -> void:
	if OS.is_debug_build():
		debug_utils(event)


func debug_utils(event: InputEvent) -> void:
	if event.is_action_pressed("debug_restart"):
		# warning-ignore:return_value_discarded
		get_tree().reload_current_scene()
		$Player.clear_tail()
	if event.is_action_pressed("debug_add_addon"):
		$Player.push_tail(load("res://addons/Addon.tscn").instance().set_shooter(debug_shooter.instance()))


func _safely_queue_free(node: Node) -> void:
	if node == null:
		return
	node.queue_free()


func _on_Player_game_over():
	if game_over:
		return
	$Player.input = Vector2.ZERO
	game_over = true

	$Player/Shooter.auto_shoot = false
	$MobSpawner.stop_spawning()
	yield($Player.clear_tail(), "completed")

	add_child(RestartMessage.instance())


func _on_MobSpawner_mob_killed():
	kills += 1

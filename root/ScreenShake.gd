# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

extends Camera2D


var shaking_active: = false
var strength_low: float
var strength_high: float
var rng = RandomNumberGenerator.new()


func _ready() -> void:
	# warning-ignore:return_value_discarded
	Effect.connect("screen_shake", self, "on_screen_shake")


func on_screen_shake(duration: float, strength: float) -> void:
	shaking_active = true
	strength_low = - floor(strength / 2)
	strength_high = ceil(strength / 2)
	$Duration.start(duration)


func _process(_delta: float) -> void:
	if not shaking_active:
		return

	offset = Vector2(
			rng.randi_range(strength_low, strength_high),
			rng.randi_range(strength_low, strength_high))


func _on_Duration_timeout():
	shaking_active = false
	offset = Vector2.ZERO

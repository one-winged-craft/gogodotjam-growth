# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

extends ParallaxBackground


export (int) var speed: = 10


var x_offset: = 0.0


func _physics_process(delta: float) -> void:
	x_offset += delta

	scroll_offset = Vector2.LEFT * speed * x_offset

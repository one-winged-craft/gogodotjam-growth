# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name Bat, "res://mobs/bat/BatIdle.png"
extends Mob


export (int) var shot: = 4


var current_shot: = 1
var left_shot: = shot
var _load_time: SceneTreeTimer = null


func _spawn() -> void:
	position = screen_size * vanguard
	if randi() % 2:
		position.y = - size
		_direction = Vector2.DOWN
	else:
		position.y = screen_size.y + size
		_direction = Vector2.UP

	_load_time = get_tree().create_timer(screen_size.y / shot / speed)


func _move(delta: float) -> void:
	var target: Vector2 = get_tree().get_nodes_in_group("weakpoint")[0].global_position
	if _is_in(position.y, target.y - size, target.y + size):
		$Shooter.auto_shoot = true
	if _load_time.time_left <= 0.0 and current_shot < shot:
		$Shooter.auto_shoot = true
		current_shot += 1
		_load_time = get_tree().create_timer(screen_size.y / shot / speed)
	else:
		$Shooter.auto_shoot = false

	position += _direction * speed * delta
	sine_move(Vector2.LEFT, position.x)
	sine_move(Vector2.UP, position.y)


func _is_in(value: float, start: float, end: float) -> bool:
	return value > start and value < end


func _on_Shooter_shot() -> void:
	Effect.one_shot_anim($Shooter, "Base_muzzle")

# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name Dragon, "res://mobs/dragon/DragonShoot.png"
extends Mob


export (float) var load_time: = 3


onready var _load_timer: SceneTreeTimer = null
onready var _shoot_timer: = get_tree().create_timer(0.0)


func _spawn() -> void:
	position = Vector2(
		screen_size.x + size + 40,
		randi_range(size, screen_size.y - size))

	target = Vector2(vanguard.x * screen_size.x,
		randi_range(0 + vanguard.y * screen_size.y, screen_size.y - vanguard.y * screen_size.y))

	_direction = position.direction_to(target)


func _move(delta: float) -> void:
	if position.x > target.x:
		sine_move(Vector2.UP, position.y)
		position += _direction * speed * delta
		return

	if _shoot_timer.time_left <= 0.0:
		$Sprite.play("load")

	if _load_timer == null:
		_load_timer = get_tree().create_timer(load_time)

	if _load_timer.time_left <= 0.0:
		$Shooter.auto_shoot = true


func _on_Shooter_shot():
	$Sprite.play("shoot")
	_shoot_timer = get_tree().create_timer(0.3)

	_shot_times += 1
	if _shot_times == salve:
		$Shooter.auto_shoot = false
		_shot_times = 0
		_load_timer = get_tree().create_timer(load_time)

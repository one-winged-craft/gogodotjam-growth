# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name WormChild, "res://mobs/worm/WormChild.png"
extends Worm


func _spawn():
	pass


func at(position: Vector2, direction: Vector2) -> WormChild:
	self.position = position
	_direction = direction
	return self


func take_damage(amount: int) -> void:
	life -= amount
	if life <= 0:
		if loot != null:
			self.get_parent().call_deferred("add_child",
					loot.instance().to(self.position))
		self.queue_free()
		emit_signal("killed")

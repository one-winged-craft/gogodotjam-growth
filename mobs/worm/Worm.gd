# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name Worm, "res://mobs/worm/Worm.png"
extends Mob


export (PackedScene) var TailSprites: = preload("res://mobs/worm/Tails/WormTail.tscn")
export (int) var tail_length: = 5
export (int) var max_speed: = 160
export (int) var acceleration: = 5
export (float) var rotation_speed: = PI / 50
export (PackedScene) var WormChild


var part_direction: = []


func _ready() -> void:
	for frame_index in tail_length:
		var part: AnimatedSprite = TailSprites.instance()
		var part_outline: AnimatedSprite = TailSprites.instance()
		part_outline.animation = "outline"

		part.frame = frame_index
		part_outline.frame = part.frame
		part.z_index = - frame_index
		part_outline.z_index = part.z_index
		part.position = - _direction * size / 2 * frame_index
		part_outline.position = part.position

		$Tail.add_child(part)
		$TailOutline.add_child(part_outline)


func _spawn() -> void:
	position = screen_size * vanguard
	if randi() % 2:
		position.y = - size
		_direction = Vector2.DOWN
	else:
		position.y = screen_size.y + size
		_direction = Vector2.UP


func _move(delta: float) -> void:
	speed = int(min(max_speed, speed + acceleration))

	target = _get_target()
	_target(target)


	for count in range($Tail.get_child_count() - 1, -1, -1):
		var part: AnimatedSprite = $Tail.get_child(count)
		if count == 0:
			part.global_position = global_position - _direction * size / 2
			$TailOutline.get_child(count).global_position = part.global_position
		else:
			var part_before: AnimatedSprite = $Tail.get_child(count - 1)
			part.global_position = part_before.global_position - size / 2 * _direction # UGLY MOVEMENT
			$TailOutline.get_child(count).global_position = part.global_position
		part.material = $Sprite.material
	position += _direction * speed * delta

func take_damage(amount: int) -> void:
	.take_damage(amount)
	if life <= 0 and WormChild != null:
		for direction in [Vector2.ONE, Vector2(1, -1)]:
			get_parent().call_deferred("add_child", 
					WormChild.instance().at(global_position, direction))

	_direction = Vector2.RIGHT


func _target(point: Vector2) -> void:
	_direction = _direction.rotated(
			_direction.angle_to(
			position.direction_to(point)) * rotation_speed)


func _get_target() -> Vector2:
	var player: Node2D = get_tree().get_nodes_in_group("weakpoint")[0]
	if (not is_instance_valid(player)) \
			 or player.global_position.x + 2 * size >= position.x:
		return Vector2(- screen_size.x * 1.1, 0)
	return player.global_position

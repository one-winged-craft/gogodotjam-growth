# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name MobSpawner
extends Node


signal mob_killed


var is_spawning: = false
var kills: = 0
var mobs: = {
	"base": {
		"instance": preload("res://mobs/base/Mob.tscn"),
		"max_at_once": 3,
		"spawn_rate": 12,
	},
	"bat": {
		"instance": preload("res://mobs/bat/Bat.tscn"),
		"max_at_once": 2,
		"spawn_rate": 20,
	},
	"dragon": {
		"instance": preload("res://mobs/dragon/Dragon.tscn"),
		"max_at_once": 1,
		"spawn_rate": 60,
	},
	"stalker": {
		"instance": preload("res://mobs/stalker/Stalker.tscn"),
		"max_at_once": 1,
		"spawn_rate": 35,
	},
	"worm": {
		"instance": preload("res://mobs/worm/Worm.tscn"),
		"max_at_once": 2,
		"spawn_rate": 20,
	},
}


func _ready() -> void:
	for mob in mobs:
		mobs[mob]["timer"] = get_tree().create_timer(mobs[mob]["spawn_rate"])
	mobs["base"]["timer"] = get_tree().create_timer(0.0)


func stop_spawning() -> void:
	is_spawning = false


func start_spawning() -> void:
	is_spawning = true


func _process(_delta: float) -> void:
	if not is_spawning:
		return
	for mob in mobs:
		if mobs[mob]["timer"].time_left <= 0.0:
			for i in randi() % mobs[mob]["max_at_once"] + 1:
				var new_mob: Mob = mobs[mob]["instance"].instance()
				# warning-ignore:return_value_discarded
				new_mob.connect("killed", self, "_on_Mob_killed")
				self.add_child(new_mob)
			mobs[mob]["timer"] = get_tree().create_timer(mobs[mob]["spawn_rate"])


func _on_Mob_killed() -> void:
	emit_signal("mob_killed")

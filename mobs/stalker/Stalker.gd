# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name Stalker, "res://mobs/stalker/StalkerIdle.png"
extends Mob


onready var _shoot_timer: = get_tree().create_timer(0.0)


func _spawn() -> void:
	target = _get_target()
	position = Vector2(screen_size.x + size, target.y)
	vanguard.x = 0.8 + randf()/ 10


func _move(delta: float) -> void:
	if position.x <= target.x:
		_direction.x = 0.0
	else:
		_direction.x = - 4
	target = _get_target()
	_direction.y = sign(target.y - position.y)
	position += _direction * speed * delta
	sine_move(Vector2.UP, position.y)

	if _shoot_timer.time_left <= 0.0:
		$Sprite.play("default")


func _get_target() -> Vector2:
	var player: Node2D = get_tree().get_nodes_in_group("weakpoint")[0]
	return Vector2(screen_size.x * vanguard.x, player.global_position.y)


func _on_Shooter_shot() -> void:
	$Sprite.play("shoot")
	_shoot_timer = get_tree().create_timer(0.1)
	Effect.one_shot_anim($Shooter, "Stalker_muzzle")

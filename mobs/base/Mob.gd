# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name Mob, "res://mobs/bat/BatIdle.png"
extends KinematicBody2D


signal killed


const color_swap_material: = preload("res://effects/ColorSwapMaterial.tres")


export (PackedScene) var loot
export (int) var life: = 3
export (float) var speed: = 30
export (Vector2) var vanguard: = Vector2(0.8, 0)
export (int) var salve: = 3
export (float) var sine_radius: = 1
export (float) var sine_frequency: = 600
export (float) var wait_time: = 1.0


var _direction: = Vector2.LEFT
var _shot_times: = 0
var _elapsed_time: = 0.0


onready var size: float = $Hitbox.shape.radius
onready var screen_size: = get_viewport().get_visible_rect().size
onready var target: = Vector2(
		screen_size.x * vanguard.x,
		randi_range(size, screen_size.y - size))
onready var _flash_timer: = get_tree().create_timer(0.0)
onready var _wait_timer: SceneTreeTimer = null


func _ready() -> void:
	# warning-ignore:return_value_discarded
	$CollisionArea.connect("body_entered", self, "_on_CollisionArea_entered")

	$Sprite.material = color_swap_material.duplicate()
	$Sprite.material.set_shader_param("old_color", Color8(0, 0, 0))

	_spawn()


func _spawn() -> void:
	position = Vector2(
			screen_size.x + size,
			target.y)
	speed = randi_range(15.0, 25.0)


func _physics_process(delta: float) -> void:
	_elapsed_time += delta
	if _flash_timer.time_left <= 0.0:
		$Sprite.material.set_shader_param("new_color", Color8(0, 0, 0))
	_move(delta)


func _move(delta: float) -> void:
	if position.x <= screen_size.x * vanguard.x:
		position.x = screen_size.x * vanguard.x + 0.1
		_direction = Vector2.ZERO

		if _wait_timer == null:
			_wait_timer = get_tree().create_timer(wait_time)


	if is_instance_valid(_wait_timer) and _wait_timer.time_left <= 0.0:
		_wait_timer = null
		$Shooter.auto_shoot = true

	position += _direction * speed * delta
	sine_move(Vector2.UP, position.y)


static func randi_range(minimum: float, maximum: float) -> int:
	if minimum > maximum:
		return 0
	return randi() % int(maximum - minimum) + int(minimum)


func sine_move(direction: Vector2, offset: float) -> void:
	match direction:
		Vector2.UP, Vector2.DOWN:
			position.y = sin(deg2rad(
					position.x + sine_frequency * _elapsed_time)) * sine_radius + offset
		Vector2.RIGHT, Vector2.LEFT:
			position.x = cos(deg2rad(
					position.y + sine_frequency * _elapsed_time)) * sine_radius + offset


func take_damage(amount: int) -> void:
	life -= amount
	if life <= 0:
		if loot != null:
			self.get_parent().call_deferred("add_child",
					loot.instance().to(self.position))
		self.queue_free()
		emit_signal("killed")

	_flash_timer = get_tree().create_timer(0.07)
	$Sprite.material.set_shader_param("new_color", Color8(173, 67, 3))


func _on_CollisionArea_entered(body: Node) -> void:
	if body.is_in_group("walls"):
		self.queue_free()
	elif body.is_in_group("addons"):
		body.get_parent().take_damage(body.get_parent().life)
	elif body.is_in_group("weakpoint"):
		body.get_parent().die()


func _on_Shooter_shot() -> void:
	_shot_times += 1
	if _shot_times >= salve:
		$Shooter.auto_shoot = false
		_direction = Vector2.RIGHT
		$Sprite.transform *= Transform2D.FLIP_X
	Effect.one_shot_anim($Shooter, "Base_muzzle")

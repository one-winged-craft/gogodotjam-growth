# SPDX-FileCopyrightText: 2021 Adriel Dumas--Jondeau
# SPDX-FileCopyrightText: 2021 Luc Deligne
#
# SPDX-License-Identifier: MIT

class_name Loot
extends Area2D


const addon: = preload("res://addons/Addon.tscn")


export (Dictionary) var shooters = {
	null: 1,
	preload("res://guns/addons/absorber/AbsorberShooter.tscn"): 0,
	preload("res://guns/addons/laser/LaserShooter.tscn"): 0,
	preload("res://guns/addons/missile/MissileShooter.tscn"): 0,
	preload("res://guns/addons/pistol/PistolShooter.tscn"): 1,
	preload("res://guns/addons/spread/SpreadShooter.tscn"): 0,
}


var loot_addons: = {
	preload("res://guns/addons/absorber/AbsorberShooter.tscn"): "absorber",
	preload("res://guns/addons/laser/LaserShooter.tscn"): "laser",
	preload("res://guns/addons/missile/MissileShooter.tscn"): "missile",
	preload("res://guns/addons/pistol/PistolShooter.tscn"): "pistol",
	preload("res://guns/addons/spread/SpreadShooter.tscn"): "spread",
}


var shooter: PackedScene = null


func _ready() -> void:
	# warning-ignore:return_value_discarded
	self.connect("body_entered", self, "_on_Loot_body_entered")

	var sum: = 0
	for coeff in shooters.values():
		sum += coeff

	var result: = 1 if sum == 1 else randi() % sum + 1
	var acc: =  0
	for s in shooters:
		acc += shooters[s]
		if result <= acc:
			shooter = s
			break

	if shooter == null:
		self.queue_free()
	else:
		$Sprite.play(loot_addons[shooter])


func _physics_process(delta: float) -> void:
	position -= Vector2(50 * delta, 0)
	if global_position.x < 0 - $Hitbox.shape.radius:
		self.queue_free()


func to(position: Vector2) -> Loot:
	self.position = position
	return self


func _on_Loot_body_entered(body: Node) -> void:
	if not body.is_in_group("loot picker"):
		return

	body.push_tail(
			addon.instance().set_shooter(shooter.instance()))
	self.queue_free()
